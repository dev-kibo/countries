package com.example.projekat;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainViewModel extends ViewModel {
    private MutableLiveData<String> countryName;
//    private MutableLiveData<AppState> appState;
    private MutableLiveData<FirebaseUser> user;

    public MainViewModel() {
//        this.appState = new MutableLiveData<>(AppState.USER_UNAUTHENTICATED);
        this.countryName = new MutableLiveData<>();
        this.user = new MutableLiveData<>(FirebaseAuth.getInstance().getCurrentUser());
    }

    public MutableLiveData<FirebaseUser> getUser() {
        return user;
    }

    public MutableLiveData<String> getCountryName() {
        return countryName;
    }

//    public MutableLiveData<AppState> getAppState() {
//        return appState;
//    }
}
