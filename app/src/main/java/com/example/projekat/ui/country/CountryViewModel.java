package com.example.projekat.ui.country;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.projekat.models.Country;
import com.example.projekat.utils.JSONParser;

import org.json.JSONArray;

import java.util.List;

public class CountryViewModel extends AndroidViewModel {

    private MutableLiveData<Country> country;

    public CountryViewModel(@NonNull Application application) {
        super(application);
        country = new MutableLiveData<>();

    }

    public MutableLiveData<Country> getCountry() {
        return country;
    }

    void loadData(String countryName) {
        // Url
        String url = "https://restcountries.eu/rest/v2/name/" + countryName.toLowerCase() + "?fullText=true";

        // Zahtjev
        RequestQueue requestQueue = Volley.newRequestQueue(getApplication().getApplicationContext());

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    List<Country> list = JSONParser.parseJSONArray(response);
                    country.setValue(list.get(0));
                } catch (Exception e) {e.printStackTrace();};
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Error
                try {
                    country.setValue(null);
                } catch (Exception json) {};
            }
        });

        requestQueue.add(jsonArrayRequest);
    }
}
