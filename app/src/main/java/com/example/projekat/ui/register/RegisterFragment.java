package com.example.projekat.ui.register;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.projekat.MainViewModel;
import com.example.projekat.R;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterFragment extends Fragment {

    private RegisterViewModel viewModel;
    private MainViewModel mainViewModel;
    private TextView labelError;
    private TextView labelErrorUsername;
    private TextView labelErrorEmail;
    private TextView labelErrorPassword;
    private EditText inputUsername;
    private EditText inputEmail;
    private EditText inputPassword;
    private Button btnSignup;
    private ProgressBar progressBar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Uzimamo view modele
        viewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_register, container, false);

        // Init
        labelError = root.findViewById(R.id.label_signup_error_msg);
        labelErrorUsername = root.findViewById(R.id.label_signup_username_error);
        labelErrorEmail = root.findViewById(R.id.label_signup_email_error);
        labelErrorPassword = root.findViewById(R.id.label_signup_password_error);

        inputUsername = root.findViewById(R.id.input_signup_username);
        inputEmail = root.findViewById(R.id.input_signup_email);
        inputPassword = root.findViewById(R.id.input_signup_password);

        progressBar = root.findViewById(R.id.registerProgressBar);
        btnSignup = root.findViewById(R.id.btn_signup_signup);

        return root;
    }

    private void cleanMessages() {
        labelError.setText("");
        labelErrorEmail.setText("");
        labelErrorUsername.setText("");
        labelErrorPassword.setText("");
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Slusamo kada korisnik klikne da se registruje
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Uklanjamo obavestenja o greskama
                cleanMessages();

                // Uzimamo podatke
                String username = inputUsername.getText().toString();
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();

                //Kreiramo nalog
                progressBar.setVisibility(View.VISIBLE);
                viewModel.createAccount(username, email, password);
            }
        });

        // Slusamo za promjene u stanju register view modela
        // Koristimo da update UI
        viewModel.getRegistrationState().observe(getViewLifecycleOwner(), new Observer<RegistrationState>() {
            @Override
            public void onChanged(RegistrationState registrationState) {
                progressBar.setVisibility(View.GONE);
                switch (registrationState) {
                    case INVALID_EMAIL:
                        labelErrorEmail.setText(R.string.label_signup_error_email);
                        break;
                    case INVALID_EMPTY_FIELDS:
                        labelError.setText(R.string.label_signup_error_empty);
                        break;
                    case INVALID_PASSWORD:
                        labelErrorPassword.setText(R.string.label_signup_error_password);
                        break;
                    case USER_ALREADY_EXISTS:
                        labelErrorEmail.setText(R.string.label_signup_error_user_exists);
                        break;
                    case REGISTRATION_FAILED:
                        labelError.setText(R.string.label_signup_error_account);
                        break;
                    case REGISTRATION_SUCCESS:
                        // Stavljamo glavno stanje aplikacije da je korisnik prijavljen
                        mainViewModel.getUser().setValue(FirebaseAuth.getInstance().getCurrentUser());
                        Navigation.findNavController(view).popBackStack(R.id.nav_home, false);
                }
            }
        });
    }
}
