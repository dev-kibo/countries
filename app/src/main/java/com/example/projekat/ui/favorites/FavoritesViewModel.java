package com.example.projekat.ui.favorites;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONObject;

public class FavoritesViewModel extends ViewModel {
    private MutableLiveData<JSONArray> countries;
    private FirebaseFirestore fStore;

    public FavoritesViewModel() {
        countries = new MutableLiveData<>();
        fStore = FirebaseFirestore.getInstance();
        loadData();
    }

    public MutableLiveData<JSONArray> getCountries() {
        return countries;
    }

    private void loadData() {
        // Uzimanje iz firestore
        fStore.collection("users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("LISTEN_ERROR", "Listen failed.", e);
                    return;
                }
                if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                    JSONArray arr = new JSONArray();

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    for(QueryDocumentSnapshot document : queryDocumentSnapshots) {
                        if (document.getData().get("userId").equals(user.getUid())) {
                            JSONObject obj = new JSONObject(document.getData());
                            arr.put(obj);
                        }
                    }
                    // Ako postoje favoriti postavi ih
                    if (arr.length() > 0) {
                        countries.setValue(arr);
                    } else {
                        countries.setValue(null);
                    }
                } else {
                    countries.setValue(null);
                }
            }
        });
    }
}
