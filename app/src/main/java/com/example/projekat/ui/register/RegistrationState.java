package com.example.projekat.ui.register;

public enum RegistrationState {
    INVALID_EMAIL,
    INVALID_PASSWORD,
    USER_ALREADY_EXISTS,
    REGISTRATION_FAILED,
    INVALID_EMPTY_FIELDS,
    REGISTRATION_SUCCESS
}
