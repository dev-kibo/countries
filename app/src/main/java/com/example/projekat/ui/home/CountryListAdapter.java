package com.example.projekat.ui.home;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.example.projekat.MainViewModel;
import com.example.projekat.R;
import com.example.projekat.models.Country;

import java.util.List;

// Adapter za prikaz drzava na Home
public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.CountryViewHolder> {
    private List<Country> countries;
    private Activity context;
    private RecyclerView recyclerView;
    private MainViewModel mainViewModel;

    //Svaki view holder sadrzi jednu drzavu za prikaz
    class CountryViewHolder extends RecyclerView.ViewHolder {
        TextView countryName;
        TextView countryRegion;
        TextView countryCapital;
        ImageView flag;

        CountryViewHolder(@NonNull View itemView) {
            super(itemView);

            countryName = itemView.findViewById(R.id.country_name);
            countryCapital = itemView.findViewById(R.id.country_capital);
            countryRegion = itemView.findViewById(R.id.country_region);
            flag = itemView.findViewById(R.id.country_flag);
        }

    }

    public CountryListAdapter(List<Country> countries, Activity context, MainViewModel mainViewModel) {
        this.countries = countries;
        this.context = context;
        this.mainViewModel = mainViewModel;
    }

    @NonNull
    @Override
    public CountryListAdapter.CountryViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {


        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country, parent, false);
        // Kada korisnik klikne na drzavu uzima se njeno ime i postavlja u Main view model
        // da bi se proslijedilo fragmentu koji prikazuje tu drzavu
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Uzimanje pozicije kliknute drzave
                int item = recyclerView.getChildAdapterPosition(view);
                Country country = countries.get(item);
                mainViewModel.getCountryName().setValue(country.getName());
                // Navigacija na stranicu za prikaz kliknute drzave
                Navigation.findNavController(context, R.id.nav_host_fragment).navigate(R.id.countryFragment);
            }
        });

        // Za svaku drzavu vraca se po jedna instanca, ali ne vise od 10etak odjednom
        return new CountryViewHolder(view);
    }

    // Sluzi da uzme referencu na recylerview da bi mogli uzeti poziciju kliknutog elementa
    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        this.recyclerView = recyclerView;
    }

    @Override
    public void onBindViewHolder(@NonNull CountryListAdapter.CountryViewHolder holder, int position) {
        Country country = countries.get(position);

        // Postavljanje vrijednosti iz proslijedjenog data seta u view holder
        holder.countryName.setText(country.getName());
        holder.countryCapital.setText(context.getString(R.string.placeholder_country_capital_is, country.getCapital()));
        holder.countryRegion.setText(context.getString(R.string.placeholder_country_in, country.getRegion()));
        // Plugin za prikaz svg slika
        SvgLoader.pluck().with(context).load(country.getFlag(), holder.flag);
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }
}
