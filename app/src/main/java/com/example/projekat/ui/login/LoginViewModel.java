package com.example.projekat.ui.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginViewModel extends ViewModel {

    // Stanje korsinika prilikom logina
    private MutableLiveData<AuthenticationState> authenticationState;
    private FirebaseAuth fAuth;

    public LoginViewModel() {
        authenticationState = new MutableLiveData<>();
        fAuth = FirebaseAuth.getInstance();
    }

    MutableLiveData<AuthenticationState> getAuthenticationState() {
        return authenticationState;
    }

    private boolean areCredentialsProvided(String email, String password) {
        return !email.equals("") && !password.equals("");
    }

    void authenticate(String email, String password) {
        // Ako polja nisu prazna izvrsiti login
        if (areCredentialsProvided(email, password)) {
            fAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        authenticationState.setValue(AuthenticationState.AUTHENTICATION_SUCCESS);
                    } else {
                        authenticationState.setValue(AuthenticationState.AUTHENTICATION_FAILED);
                    }
                }
            });
        } else {
            authenticationState.setValue(AuthenticationState.INVALID_EMPTY_FIELDS);
        }
    }
}
