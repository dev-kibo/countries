package com.example.projekat.ui.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekat.MainViewModel;
import com.example.projekat.R;
import com.example.projekat.models.Country;
import com.example.projekat.ui.home.CountryListAdapter;
import com.example.projekat.utils.JSONParser;

import org.json.JSONArray;

import java.util.List;

public class SearchFragment extends Fragment {

    private SearchViewModel searchViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private MainViewModel mainViewModel;
    private ProgressBar progressBar;
    private TextView labelNoData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchViewModel = new ViewModelProvider(requireActivity()).get(SearchViewModel.class);
        // Main view model sadrzi ime drzave na koju je kliknuto
        // Proslijedujemo ga adapteru
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_search, container, false);
        // Povezivanje recycler viewa
        recyclerView = root.findViewById(R.id.fragment_search);
        layoutManager = new LinearLayoutManager(requireActivity());
        recyclerView.setLayoutManager(layoutManager);

        progressBar = root.findViewById(R.id.searchProgressBar);
        labelNoData = root.findViewById(R.id.label_search_no_data);

        // Slusanje kada ce rezultati pretrage da stignu
        searchViewModel.getResults().observe(getViewLifecycleOwner(), new Observer<JSONArray>() {
            @Override
            public void onChanged(JSONArray jsonArray) {
                progressBar.setVisibility(View.GONE);
                if (labelNoData.getVisibility() == View.VISIBLE) {
                    labelNoData.setVisibility(View.GONE);
                }
                loadData(jsonArray);
            }
        });

        return root;
    }

    private void loadData(JSONArray jsonArray) {
        // Ako nema rezultata ispisati poruku
        if (jsonArray.length() > 0) {
            // Ako je view sakriven nakon sto prvi put pretraga nije vratila rezultate
            if (recyclerView.getVisibility() == View.GONE) {
                recyclerView.setVisibility(View.VISIBLE);
            }
            List<Country> results  = JSONParser.parseJSONArray(jsonArray);
            adapter = new CountryListAdapter(results, requireActivity(), mainViewModel);
            recyclerView.setAdapter(adapter);
        } else {
            // Sakrij view ako je prvi put bilo tacnih rezultata
            // a trenutno nema
            recyclerView.setVisibility(View.GONE);
            labelNoData.setVisibility(View.VISIBLE);
        }
    }
}
